import { ConvertedPage } from './app.po';

describe('converted App', function() {
  let page: ConvertedPage;

  beforeEach(() => {
    page = new ConvertedPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
